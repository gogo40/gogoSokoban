gogoSokoban
===========

An AI to solve sokobans. 

In this site you can find some interesting sokobans: http://www.sokobanonline.com/ 

Currently, this AI is experimental and need some improviment in its heuristics. I found this work http://webdocs.cs.ualberta.ca/~games/Sokoban/
and I believe can help me in this problem.

Any help is welcome!
